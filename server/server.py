import sys
import os.path
import cherrypy
from drinksController import DrinkController
from resetController import ResetController

drinks_library_directory = (os.path.abspath(os.path.join(os.path.dirname('server.py'), '..')) + '/ooapi/')
sys.path.append(drinks_library_directory)
from drinks_library import _drinks_database

class optionsController():
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    ddb = _drinks_database()

    drinkController = DrinkController(ddb=ddb)
    resetController = ResetController(ddb=ddb)

    dispatcher.connect('drink_get', '/drinks/:drink_id', controller=drinkController, action = 'GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('drinks_get_by_name', '/drinks/name/:drink_name', controller=drinkController, action = 'GET_KEY_NAME', conditions=dict(method=['GET']))
    dispatcher.connect('drinks_get_by_ingredient', '/drinks/ingredient/:ingredient_name', controller=drinkController, action = 'GET_KEY_INGREDIENT', conditions=dict(method=['GET']))
    dispatcher.connect('drink_put', '/drinks/:drink_id', controller=drinkController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('drink_delete', '/drinks/:drink_id', controller=drinkController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('drinks_get', '/drinks/', controller=drinkController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('drink_post', '/drinks/', controller=drinkController, action = 'POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('reset_put', '/reset/:drink_id', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    # CORS related options connections
    dispatcher.connect('drink_key_options', '/drinks/:drink_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('drink_name_options', '/drinks/name/:drink_name', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('drink_ingredient_options', '/drinks/ingredient/:ingredient_name', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('drink_options', '/drinks/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:drink_id', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    conf = {
        'global': {
            'server.thread_pool': 5, #optional argument
            'server.socket_host': 'localhost',
            'server.socket_port': 51070
        },
        '/': {
            'request.dispatch': dispatcher,
            'tools.CORS.on': True
        }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
