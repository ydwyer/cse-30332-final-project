import cherrypy
import re, json
from drinks_library import _drinks_database

class ResetController(object):
    def __init__(self, ddb=None):
        if ddb is None:
            self.ddb = _drinks_database()
        else:
            self.ddb = ddb

    # When PUT request comes in to /reset/ endpoint, then the drinks database is reloaded 
    def PUT_INDEX(self):
        output = {'result':'success'}
        
        self.ddb.__init__()
        self.ddb.load_drinks('data.dat')
        
        return json.dumps(output)

    # When PUT request comes in for /reset/drink_id endpoint, then that drink is reloaded and updated in ddb
    def PUT_KEY(self, drink_id):
        output = {'result':'success'}
        did = int(drink_id)

        try:
            ddbtmp = _drinks_database()
            ddbtmp.load_drinks('data.dat')

            drink = ddbtmp.get_drink(did)
            
            self.ddb.set_drink(did, drink)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
