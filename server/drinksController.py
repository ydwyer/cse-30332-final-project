import cherrypy
import re, json
import sys
import os.path

drinks_library_directory = (os.path.abspath(os.path.join(os.path.dirname('server.py'), '..'))
+ '/ooapi/')
sys.path.append(drinks_library_directory)
from drinks_library import _drinks_database

class DrinkController(object):
    def __init__(self, ddb=None):
        if ddb is None:
            self.ddb = _drinks_database()
        else:
            self.ddb = ddb

        self.ddb.load_drinks('data.dat')

    # When GET request for /drinks/drink_id comes in, then we respond with json string
    def GET_KEY(self, drink_id):
        output = {'result': 'success'}
        drink_id = int(drink_id)

        try:
            drink = self.ddb.get_drink(drink_id)
            if drink is not None:
                output['id'] = drink_id
                output['name'] = drink[0]
                output['alternate name'] = drink[1]
                output['tags'] = drink[2]
                output['videos'] = drink[3]
                output['categories'] = drink[4]
                output['iba'] = drink[5]
                output['alcoholic'] = drink[6]
                output['glass'] = drink[7]
                output['instructions'] = drink[8]
                output['thumbnails'] = drink[9]
                output['ingredient1'] = drink[10]
                output['ingredient2'] = drink[11]
                output['ingredient3'] = drink[12]
                output['ingredient4'] = drink[13]
                output['ingredient5'] = drink[14]
                output['ingredient6'] = drink[15]
                output['ingredient7'] = drink[16]
                output['ingredient8'] = drink[17]
                output['ingredient9'] = drink[18]
                output['ingredient10'] = drink[19]
                output['ingredient11'] = drink[20]
                output['ingredient12'] = drink[21]
                output['ingredient13'] = drink[22]
                output['ingredient14'] = drink[23]
                output['ingredient15'] = drink[24]
                output['measure1'] = drink[25]
                output['measure2'] = drink[26]
                output['measure3'] = drink[27]
                output['measure4'] = drink[28]
                output['measure5'] = drink[29]
                output['measure6'] = drink[30]
                output['measure7'] = drink[31]
                output['measure8'] = drink[32]
                output['measure9'] = drink[33]
                output['measure10'] = drink[34]
                output['measure11'] = drink[35]
                output['measure12'] = drink[36]
                output['measure13'] = drink[37]
                output['measure14'] = drink[38]
                output['measure15'] = drink[39]
            else:
                output['result'] = 'error'
                output['message'] = 'drink not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # When GET request for /drinks/name/drink_name comes in, then we respond with json string of all drinks whose names contain the given drink name
    def GET_KEY_NAME(self, drink_name):
        output = {'result': 'success'}
        output['drinks'] = []

        try:
            for drink_id in self.ddb.get_drinks_by_name(drink_name):
                drink_id = int(drink_id)
                drink = self.ddb.get_drink(drink_id)

                d = {}
                d['id'] = drink_id
                d['name'] = drink[0]
                d['alternate name'] = drink[1]
                d['tags'] = drink[2]
                d['videos'] = drink[3]
                d['categories'] = drink[4]
                d['iba'] = drink[5]
                d['alcoholic'] = drink[6]
                d['glass'] = drink[7]
                d['instructions'] = drink[8]
                d['thumbnails'] = drink[9]
                d['ingredient1'] = drink[10]
                d['ingredient2'] = drink[11]
                d['ingredient3'] = drink[12]
                d['ingredient4'] = drink[13]
                d['ingredient5'] = drink[14]
                d['ingredient6'] = drink[15]
                d['ingredient7'] = drink[16]
                d['ingredient8'] = drink[17]
                d['ingredient9'] = drink[18]
                d['ingredient10'] = drink[19]
                d['ingredient11'] = drink[20]
                d['ingredient12'] = drink[21]
                d['ingredient13'] = drink[22]
                d['ingredient14'] = drink[23]
                d['ingredient15'] = drink[24]
                d['measure1'] = drink[25]
                d['measure2'] = drink[26]
                d['measure3'] = drink[27]
                d['measure4'] = drink[28]
                d['measure5'] = drink[29]
                d['measure6'] = drink[30]
                d['measure7'] = drink[31]
                d['measure8'] = drink[32]
                d['measure9'] = drink[33]
                d['measure10'] = drink[34]
                d['measure11'] = drink[35]
                d['measure12'] = drink[36]
                d['measure13'] = drink[37]
                d['measure14'] = drink[38]
                d['measure15'] = drink[39]

                output['drinks'].append(d)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # When GET request for /drinks/ingredient/ingredient_name comes in, then we respond with json string of all drinks containing the given ingredient
    def GET_KEY_INGREDIENT(self, ingredient_name):
        output = {'result': 'success'}
        output['drinks'] = []

        try:
            for drink_id in self.ddb.get_drinks_by_ingredient(ingredient_name):
                drink_id = int(drink_id)
                drink = self.ddb.get_drink(drink_id)

                d = {}
                d['id'] = drink_id
                d['name'] = drink[0]
                d['alternate name'] = drink[1]
                d['tags'] = drink[2]
                d['videos'] = drink[3]
                d['categories'] = drink[4]
                d['iba'] = drink[5]
                d['alcoholic'] = drink[6]
                d['glass'] = drink[7]
                d['instructions'] = drink[8]
                d['thumbnails'] = drink[9]
                d['ingredient1'] = drink[10]
                d['ingredient2'] = drink[11]
                d['ingredient3'] = drink[12]
                d['ingredient4'] = drink[13]
                d['ingredient5'] = drink[14]
                d['ingredient6'] = drink[15]
                d['ingredient7'] = drink[16]
                d['ingredient8'] = drink[17]
                d['ingredient9'] = drink[18]
                d['ingredient10'] = drink[19]
                d['ingredient11'] = drink[20]
                d['ingredient12'] = drink[21]
                d['ingredient13'] = drink[22]
                d['ingredient14'] = drink[23]
                d['ingredient15'] = drink[24]
                d['measure1'] = drink[25]
                d['measure2'] = drink[26]
                d['measure3'] = drink[27]
                d['measure4'] = drink[28]
                d['measure5'] = drink[29]
                d['measure6'] = drink[30]
                d['measure7'] = drink[31]
                d['measure8'] = drink[32]
                d['measure9'] = drink[33]
                d['measure10'] = drink[34]
                d['measure11'] = drink[35]
                d['measure12'] = drink[36]
                d['measure13'] = drink[37]
                d['measure14'] = drink[38]
                d['measure15'] = drink[39]

                output['drinks'].append(d)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # When PUT request for /drinks/drink_id comes in, then we change that drink in the ddb
    def PUT_KEY(self, drink_id):
        output = {'result': 'success'}
        drink_id = int(drink_id)

        data = json.loads(cherrypy.request.body.read().decode('utf-8'))

        drink = list()
        drink.append(data['name'])
        drink.append(data['alternate name'])
        drink.append(data['tags'])
        drink.append(data['videos'])
        drink.append(data['categories'])
        drink.append(data['iba'])
        drink.append(data['alcoholic'])
        drink.append(data['glass'])
        drink.append(data['instructions'])
        drink.append(data['thumbnails'])
        drink.append(data['ingredient1'])
        drink.append(data['ingredient2'])
        drink.append(data['ingredient3'])
        drink.append(data['ingredient4'])
        drink.append(data['ingredient5'])
        drink.append(data['ingredient6'])
        drink.append(data['ingredient7'])
        drink.append(data['ingredient8'])
        drink.append(data['ingredient9'])
        drink.append(data['ingredient10'])
        drink.append(data['ingredient11'])
        drink.append(data['ingredient12'])
        drink.append(data['ingredient13'])
        drink.append(data['ingredient14'])
        drink.append(data['ingredient15'])
        drink.append(data['measure1'])
        drink.append(data['measure2'])
        drink.append(data['measure3'])
        drink.append(data['measure4'])
        drink.append(data['measure5'])
        drink.append(data['measure6'])
        drink.append(data['measure7'])
        drink.append(data['measure8'])
        drink.append(data['measure9'])
        drink.append(data['measure10'])
        drink.append(data['measure11'])
        drink.append(data['measure12'])
        drink.append(data['measure13'])
        drink.append(data['measure14'])
        drink.append(data['measure15'])

        self.ddb.set_drink(drink_id, drink)

        return json.dumps(output)

    # When DELETE request for /drinks/drink_id comes in, we remove just that drink from the ddb
    def DELETE_KEY(self, drink_id):
        output = {'result': 'success'}
        drink_id = int(drink_id)

        self.ddb.delete_drink(drink_id)

        return json.dumps(output)

    # When GET request for /drinks/ comes in, we respond with all the drinks information in a json string
    def GET_INDEX(self):
        output = {'result': 'success'}
        output['drinks'] = []

        try:
            for drink_id in self.ddb.get_drinks():
                drink_id = int(drink_id)
                drink = self.ddb.get_drink(drink_id)

                d = {}
                d['id'] = drink_id
                d['name'] = drink[0]
                d['alternate name'] = drink[1]
                d['tags'] = drink[2]
                d['videos'] = drink[3]
                d['categories'] = drink[4]
                d['iba'] = drink[5]
                d['alcoholic'] = drink[6]
                d['glass'] = drink[7]
                d['instructions'] = drink[8]
                d['thumbnails'] = drink[9]
                d['ingredient1'] = drink[10]
                d['ingredient2'] = drink[11]
                d['ingredient3'] = drink[12]
                d['ingredient4'] = drink[13]
                d['ingredient5'] = drink[14]
                d['ingredient6'] = drink[15]
                d['ingredient7'] = drink[16]
                d['ingredient8'] = drink[17]
                d['ingredient9'] = drink[18]
                d['ingredient10'] = drink[19]
                d['ingredient11'] = drink[20]
                d['ingredient12'] = drink[21]
                d['ingredient13'] = drink[22]
                d['ingredient14'] = drink[23]
                d['ingredient15'] = drink[24]
                d['measure1'] = drink[25]
                d['measure2'] = drink[26]
                d['measure3'] = drink[27]
                d['measure4'] = drink[28]
                d['measure5'] = drink[29]
                d['measure6'] = drink[30]
                d['measure7'] = drink[31]
                d['measure8'] = drink[32]
                d['measure9'] = drink[33]
                d['measure10'] = drink[34]
                d['measure11'] = drink[35]
                d['measure12'] = drink[36]
                d['measure13'] = drink[37]
                d['measure14'] = drink[38]
                d['measure15'] = drink[39]

                output['drinks'].append(d)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    # When POST request for /drinks/ comes in, we take the drink information from body of request, and respond with the new drink_id and more
    def POST_INDEX(self):
        output = {'result': 'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))

        drink = list()
        drink.append(data['name'])
        drink.append(data['alternate name'])
        drink.append(data['tags'])
        drink.append(data['videos'])
        drink.append(data['categories'])
        drink.append(data['iba'])
        drink.append(data['alcoholic'])
        drink.append(data['glass'])
        drink.append(data['instructions'])
        drink.append(data['thumbnails'])
        drink.append(data['ingredient1'])
        drink.append(data['ingredient2'])
        drink.append(data['ingredient3'])
        drink.append(data['ingredient4'])
        drink.append(data['ingredient5'])
        drink.append(data['ingredient6'])
        drink.append(data['ingredient7'])
        drink.append(data['ingredient8'])
        drink.append(data['ingredient9'])
        drink.append(data['ingredient10'])
        drink.append(data['ingredient11'])
        drink.append(data['ingredient12'])
        drink.append(data['ingredient13'])
        drink.append(data['ingredient14'])
        drink.append(data['ingredient15'])
        drink.append(data['measure1'])
        drink.append(data['measure2'])
        drink.append(data['measure3'])
        drink.append(data['measure4'])
        drink.append(data['measure5'])
        drink.append(data['measure6'])
        drink.append(data['measure7'])
        drink.append(data['measure8'])
        drink.append(data['measure9'])
        drink.append(data['measure10'])
        drink.append(data['measure11'])
        drink.append(data['measure12'])
        drink.append(data['measure13'])
        drink.append(data['measure14'])
        drink.append(data['measure15'])

        try:
            drink_id = int(max(self.ddb.get_drinks())) + 1
            output['id'] = drink_id

            self.ddb.set_drink(drink_id, drink)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
