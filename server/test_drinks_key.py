import unittest
import requests
import json

class TestDrinksKey(unittest.TestCase):
    SITE_URL = 'http://localhost:51070' 
    print("testing for server: " + SITE_URL)
    DRINKS_URL = SITE_URL + '/drinks/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        d = {}
        r = requests.put(self.RESET_URL, data = json.dumps(d))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_drinks_get_key(self):
        self.reset_data()
        drink_id = 15106

        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['name'], 'Apello')

    def test_drinks_get_key_name(self):
        self.reset_data()
        drink_name = "Apello"

        r = requests.get(self.DRINKS_URL + "/name/" + drink_name)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))

        test_drink = {}
        drinks = resp['drinks']
        for drink in drinks:
            if drink['id'] == 15106:
                test_drink = drink

        self.assertEqual(test_drink['name'], "Apello")

    def test_drinks_get_key_ingredient(self):
        self.reset_data()
        ingredient_name = "orange_juice"

        r = requests.get(self.DRINKS_URL + "/ingredient/" + ingredient_name)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))

        test_drink = {}
        drinks = resp['drinks']
        for drink in drinks:
            if drink['id'] == 15106:
                test_drink = drink

        self.assertEqual(test_drink['name'], "Apello")

    def test_drinks_put_key(self):
        self.reset_data()
        drink_id = 15106

        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['name'], 'Apello')

        d = resp
        d['name'] = 'Something Else'
        r = requests.put(self.DRINKS_URL + str(drink_id), data = json.dumps(d))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['name'], d['name'])

    def test_drinks_delete_key(self):
        self.reset_data()
        drink_id = 15106

        r = requests.delete(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.DRINKS_URL + str(drink_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
    unittest.main()
