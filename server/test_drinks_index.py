import unittest
import requests
import json

class TestDrinksIndex(unittest.TestCase):
    SITE_URL = 'http://localhost:51070'
    print("Testing for server: " + SITE_URL)
    DRINKS_URL = SITE_URL + '/drinks/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        d = {}
        r = requests.put(self.RESET_URL, json.dumps(d))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_drinks_index_get(self):
        self.reset_data()
        r = requests.get(self.DRINKS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        test_drink = {}
        drinks = resp['drinks']
        for drink in drinks:
            if drink['id'] == 15106:
                test_drink = drink

        self.assertEqual(test_drink['name'], 'Apello')

    def test_drinks_index_post(self):
        self.reset_data()

        d = {}
        d['name'] = "New Drink"
        d['alternate name'] = None
        d['tags'] = None
        d['videos'] = None
        d['categories'] = None
        d['iba'] = None
        d['alcoholic'] = None
        d['glass'] = None
        d['instructions'] = None
        d['thumbnails'] = None
        d['ingredient1'] = None
        d['ingredient2'] = None
        d['ingredient3'] = None
        d['ingredient4'] = None
        d['ingredient5'] = None
        d['ingredient6'] = None
        d['ingredient7'] = None
        d['ingredient8'] = None
        d['ingredient9'] = None
        d['ingredient10'] = None
        d['ingredient11'] = None
        d['ingredient12'] = None
        d['ingredient13'] = None
        d['ingredient14'] = None
        d['ingredient15'] = None
        d['measure1'] = None
        d['measure2'] = None
        d['measure3'] = None
        d['measure4'] = None
        d['measure5'] = None
        d['measure6'] = None
        d['measure7'] = None
        d['measure8'] = None
        d['measure9'] = None
        d['measure10'] = None
        d['measure11'] = None
        d['measure12'] = None
        d['measure13'] = None
        d['measure14'] = None
        d['measure15'] = None
        
        r = requests.post(self.DRINKS_URL, data = json.dumps(d))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['id'], 178336)

        r = requests.get(self.DRINKS_URL + str(resp['id']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['name'], d['name'])

if __name__ == "__main__":
    unittest.main()
