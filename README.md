# CSE 30332 Final Project

## Milestone 1
### OO API
The API should can be used to manage a database of cocktails and to query that
database. The database management functions are set\_drink() and
delete\_drink(). These functions can be used to update/add new drinks and
delete a drink from the database, respectively. The querying functions are 
get\_drinks(), get\_drink(), get\_drinks\_by\_name(), and 
get\_drinks\_by\_ingredient(), respectively. These functions can be used to get
all drink IDs currently in the database, get the drink details corresponding to
a given drink ID, get all drink IDs whose drinks contain a given string in 
their name, and to get all drink IDs whose drinks contain a given ingredient, 
respectively.

To run the tests for the OO API, run the command `python3 ooapi/test_api.py`
from the base project directory (that this README is in), or run the command 
`python3 test_api.py` from the ooapi directory. This will test all of the
aforementioned functions.

### Server
#### RESTful JSON Specification
Link: https://docs.google.com/spreadsheets/d/1WU1w7xJcLCW7lHzEpAkSoZ1hOZx1u6e809qkIWV9Fi8/edit#gid=0

#### Port Number
Our web service uses the following URL: http://localhost:51070.

#### Running the Server
`python3 server.py`

#### Running the Webservice
Open the HTML file `index-home-page.html` in a web browser.

#### Testing the Server
Run the following tests:
* `python3 test_drinks_key.py`
* `python3 test_drinks_index.py`
* `python3 test_reset_endpoint.py`

## Milestone 2
### Testing the Web Client
We carefully tested each portion of the web client. For both of the search functions--name and ingredients--we searched for numerous different drinks/ingredients and compared the results with what we expected to receive based on the contents of the database. For the page supporting recipe addition/deletion/modification, we tested all three functionalities with various inputs, then ensured that they successfully propagated to the database both by inspecting the database and by querying it via the website. To test the page which displays drink details, we accessed it for a wide array of drinks with different numbers of ingredients and lengths of instructions and names and made sure the data displayed therein matched what was expected based on the database.

### User Interaction
#### Home Page
* First, the user can choose whether to search for a drink by drink name or ingredient by clicking on the corresponding radio button.
* Then, the user can type in a search term in the search box.
* Finally, the user can click on the search button.
* If drink(s) corresponding to the search term were found, they will be displayed in a list, with an image of the drink on the left and the drink name on the right.
* If the user clicks on the drink name, it will direct them to another web page with more specific information about the drink.
* If no drinks corresponding to the search term were found, the message "Sorry! No drinks were found." will be displayed.
* When the clear button is clicked, the web page will be reloaded.

#### Upload Recipe Page
* First, the user can click on "Upload Recipe" on the home page, which will take them to another web page where they can upload their own drink recipe to the database.
* Then, the user can enter information about the drink, including the drink name, type of glass, list of ingredients, list of measures, alcoholic content, instructions, and link to a picture of the drink.
* Finally, the user can click on the "Upload Recipe" button.
* If the drink was successfully uploaded to the database, the message "Success! Your recipe was added to the database." will be displayed.
* If the drink was not uploaded to the database, the message "Sorry! An error occurred. Please try again." will be displayed.
* When the reset button is clicked, the web page will be reloaded.

#### Modify Recipe Page
* The Modify Recipe page is interacted with in the same way as the Upload Recipe page, but the user additionally specifies the ID of the recipe they would like to modify.
* If the user wishes not to modify a given attribute, they may simply leave that field blank and the attribute will remain unchanged.

#### Delete Recipe Page
* First, the user can click on "Delete Recipe" on the home page, which will take them to another web page where they can delete drink recipes from the database.
* The user then can enter the ID of the recipe they would like to delete.
* The user then can click on the "Delete Recipe" button to attempt to delete the relevant recipe.
* If the drink was successfully deleted from the database, the message "Success! The specified recipe has been deleted." will be displayed.
* If the drink was not successfully deleted from the database, the message "Sorry! An error occurred. Please try again." will be displayed.
* When the reset button is clicked, the web page will be reloaded.

## Complexity
The OO API portion of the project includes nine functions, which support all of the requests that can possibly be made by our server. The API works on a list of drink recipes, each of which contains over 30 attributes, so functions that involve adding or modifying recipes have to correctly set a relatively large number of values. Our database contains several hundred drinks, and many thousand of lines, so the functions which involve querying it must run efficiently. We have had no troubles with execution time, so we imagine that our project could easily support a database of even greater scale. Each of the functions that act on the database are tested by the `test_api.py` file.

The server portion of our project include two controllers which support nine combined requests. All four of the main types of requests we have discussed this semester (PUT, POST, GET, DELETE) are represented in the requests supported by our controllers. Some of our requests are of fairly large scale and involve sending over 30 attributes, again because of the relatively large number of attributes required to describe a drink recipe in our database. Our server supports all of the request types that can be sent by the controllers and also implements CORS as well as the OPTIONS connections required by CORS.

The frontend portion of our project includes five HTML files and a JavaScript file corresponding to each of the HTML files. Our five pages allow for querying our database, viewing a drink in detail, and uploading, updating, or deleting a drink. Four of those pages take user input, and two of them--the pages for updating (`index-modify-recipe.html`) and uploading recipes (`index-upload-recipe.html`)--take, process, and use a fair amount of it (drink name, glass type, ingredients, ingredient amount, alcohol content, instructions, buttons). The main search page (`index-home-page.html`) dynamically updates based on search results, and as such has fairly complex JavaScript behind it. All five of the pages involve making network calls to the server, and the modify (`index-modify-recipe.html`) and delete drink (`index-delete-recipe.html`) pages involve making two, which is required in order to make sure the user is attempting to perform a valid operation. The drink display page (`index-drink-page.html`) is of a fairly large scale because it has to query for upwards of thirty attributes regarding the drink recipe it is displaying. The JavaScript for the modify recipe page (`index-modify-recipe.js`) is also fairly complex, because it has to check whether the user input a modification for each of the fields it accepts, and propagate the user's modification if they made one or make sure that the corresponding value in the drink recipe remains unchanged if they did not. It also has to make sure that all of the information input by the user makes sense in terms of being part of a valid recipe.