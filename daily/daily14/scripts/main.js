console.log('page load - entered main.js for index-form.html');

var submitButton = document.getElementById('send-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var machine = document.getElementById("select-server-address").value;
    var portNumber = document.getElementById("input-port-number").value;

    var action = "GET"; // default
	if (document.getElementById('radio-get').checked){
			action = "GET";
	} else if (document.getElementById('radio-put').checked) {
			action = "PUT";
	} else if (document.getElementById('radio-post').checked) {
			action = "POST";
	} else if (document.getElementById('radio-delete').checked) {
			action = "DELETE";
	}

	var key = null;
	if (document.getElementById('checkbox-use-key').checked){
		key = document.getElementById('input-key').value;
	}

	var message_body = null;
	if (document.getElementById('checkbox-use-message').checked){
		// get key value
		message_body = document.getElementById('text-message-body').value;
	}

    makeNetworkCallToServer(machine, portNumber, action, key, message_body);

} // end of get form info

function makeNetworkCallToServer(machine, portNumber, action, key, message_body){
    console.log('entered make nw call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://" + machine + ":" + portNumber;

	if (action == "GET") {
    	xhr.open("GET", url + key, true); // 2 - associates request attributes with xhr
	}
	else if (action == "PUT") {
		xhr.open("PUT", url + key, true);
	}
	else if (action == "POST") {
		xhr.open("POST", url + key, true);
	}
	else if (action == "DELETE") {
		xhr.open("DELETE", url + key, true);
	}

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        //console.log(xhr.responseText);
        // do something
        updatePageWithResponse(action, key, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(message_body); // last step - this actually makes the request

} // end of make nw call

function updatePageWithResponse(action, key, response_text){
    var response_json = JSON.parse(response_text);

    // update a label
    var label1 = document.getElementById("response-label");
	label1.innerHTML = JSON.stringify(response_json);
	
	var label2 = document.getElementById("answer-label");

	if (key.includes("ratings")) {
		label2.innerHTML = response_json["movie_id"] + " has rating " + response_json["rating"];
	}
	else if (action == "GET" && key.length > 8) {
		label2.innerHTML = response_json["title"] + " belongs to the genres " + response_json["genres"];
	}
	else if (action == "GET") {
        for (var i = 0; i < response_json["movies"].length; i++) {
            label2.innerHTML += "<p>" + response_json["movies"][i]["title"] + "</p>";
        }
	}
	else if (action == "POST") {
		label2.innerHTML = "New movie id: " + response_json["id"];
	}
	else {
		label2.innerHTML = response_json["result"];
	}
} // end of updateAgeWithResponse
