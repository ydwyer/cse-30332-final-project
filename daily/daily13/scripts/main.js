console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var country = document.getElementById("name-text").value;
    console.log('Country you entered is ' + country);
    makeNetworkCallToCountryApi(country);

} // end of get form info

function makeNetworkCallToCountryApi(country){
    console.log('entered make nw call ' + country);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://restcountries.eu/rest/v2/name/" + country;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateCurrencyWithResponse(country, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateCurrencyWithResponse(country, response_text){
    console.log('entered updateCurrencyWithResponse ' + response_text);
    var response_json = JSON.parse(response_text);

    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json["status"] == 404){
        label1.innerHTML = 'Apologies, we could not find your country.'
    } else{
        code = response_json[0]['currencies'][0]['code'];
        label1.innerHTML = country + ' uses the currency ' + code;
        makeNetworkCallToCurrencyApi(code);
    }
} // end of updateAgeWithResponse

function makeNetworkCallToCurrencyApi(code){
    console.log('entered make nw call' + code);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.exchangerate-api.com/v4/latest/" + code;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateConversionWithResponse(code, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateConversionWithResponse(code, response_text){
    // update a label
    var rate = JSON.parse(response_text)['rates']['USD'].toFixed(2);
    var formatted_response = '1 ' + code + ' can be exchanged for ' + rate + ' USD.'

    var label2 = document.getElementById("response-line2");
    label2.innerHTML = formatted_response;

    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    var item_text = document.createTextNode(formatted_response); // creating new text
    label_item.appendChild(item_text); // adding something to button with appendChild()

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

} // end of updateTriviaWithResponse
