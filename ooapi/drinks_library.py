import json

class _drinks_database:
    def __init__(self):
        """
        Initializes all of the dicts that will contain the drink data
        for the database.

        Args:
            None

        Returns:
            None
        """
        self.drink_names = dict()
        self.drink_alternate_names = dict()
        self.drink_tags = dict()
        self.drink_videos = dict()
        self.drink_categories = dict()
        self.iba = dict()
        self.alcoholic = dict()
        self.glass = dict()
        self.instructions = dict()
        self.thumbnails = dict()

        '''
        Instead of being dicts, ingredients and measures are lists
        containing 15 dicts each, which allows for the assignment of
        their values in a loop rather than over the course of 30
        different lines, as each of the 15 ingredients/measures
        is stored in its own dict.
        '''
        self.ingredients = list()
        self.measures = list()
        for i in range(15):
            self.ingredients.append(dict())
            self.measures.append(dict())


    def load_drinks(self, drink_file):
        """
        Fills the dicts created in __init__ with values taken from a
        data file containing drink descriptions.

        Args:
            drink_file: Data file containing drink descriptions.

        Returns:
            None
        """
        f = open(drink_file)
        drink_file_json = json.load(f)["drinks"]

        for drink in drink_file_json:
            drinkid = int(drink["idDrink"])

            self.drink_names[drinkid] = drink["strDrink"]
            self.drink_alternate_names[drinkid] = drink["strDrinkAlternate"]
            self.drink_tags[drinkid] = drink["strTags"]
            self.drink_videos[drinkid] = drink["strVideo"]
            self.drink_categories[drinkid] = drink["strCategory"]
            self.iba[drinkid] = drink["strIBA"]
            self.alcoholic[drinkid] = drink["strAlcoholic"]
            self.glass[drinkid] = drink["strGlass"]
            self.instructions[drinkid] = drink["strInstructions"]
            self.thumbnails[drinkid] = drink["strDrinkThumb"]

            for i in range(15):
                ingredient_string = "strIngredient" + str(i+1)
                measure_string = "strMeasure" + str(i+1)
                self.ingredients[i][drinkid] = drink[ingredient_string]
                self.measures[i][drinkid] = drink[measure_string]


    def get_drink(self, drinkid):
        """
        Returns then data corresponding to the drink with ID=drinkid.

        Args:
            drinkid: The ID of the drink whose data should be returned.

        Returns:
            drink: A list containing all of the information associated
            with the drink that has drinkid. If the function is unable
            to acqure said information, drink=None.
        """
        try:
            name = self.drink_names[drinkid]
            alt_name = self.drink_alternate_names[drinkid]
            tags = self.drink_tags[drinkid]
            video = self.drink_videos[drinkid]
            cat = self.drink_categories[drinkid]
            iba = self.iba[drinkid]
            alc = self.alcoholic[drinkid]
            glass = self.glass[drinkid]
            inst = self.instructions[drinkid]
            thumb = self.thumbnails[drinkid]

            drink = [name, alt_name, tags, video, cat,
                     iba, alc, glass, inst, thumb]
            for i in range(15):
                drink.append(self.ingredients[i][drinkid])
            for i in range(15):
                drink.append(self.measures[i][drinkid])

        except Exception as ex:
            drink = None

        return drink

    def set_drink(self, drinkid, drink):
        """
        Sets the information corresponding to the drink with ID=drinkid
        to the value specified in drink.

        Args:
            drinkid:    The ID of the drink to be modified/created.
            drink:      List containing the values to be assigned to the
                        drink being modified.

        Returns:
            None
        """
        self.drink_names[drinkid] = drink[0]
        self.drink_alternate_names[drinkid] = drink[1]
        self.drink_tags[drinkid] = drink[2]
        self.drink_videos[drinkid] = drink[3]
        self.drink_categories[drinkid] = drink[4]
        self.iba[drinkid] = drink[5]
        self.alcoholic[drinkid] = drink[6]
        self.glass[drinkid] = drink[7]
        self.instructions[drinkid] = drink[8]
        self.thumbnails[drinkid] = drink[9]

        for i in range(15):
            '''
            The ingredients and measures are contained in
            drinks[10]->drinks[24] and drinks[25]->drinks[39],
            respectively, but need to be put in the first 15 indices
            in ingredients and measures, so a constant should be added
            to the desired ingredients/measures index to get the right
            drinks index value.
            '''
            j = i + 10
            k = i + 25
            self.ingredients[i][drinkid] = drink[j]
            self.measures[i][drinkid] = drink[k]


    def delete_drink(self, drinkid):
        """
        Deletes all information associated with a drink from the dicts.

        Args:
            drinkid: The ID of the drink to be deleted

        Returns:
            None
        """
        del(self.drink_names[drinkid])
        del(self.drink_alternate_names[drinkid])
        del(self.drink_tags[drinkid])
        del(self.drink_videos[drinkid])
        del(self.drink_categories[drinkid])
        del(self.iba[drinkid])
        del(self.alcoholic[drinkid])
        del(self.glass[drinkid])
        del(self.instructions[drinkid])
        del(self.thumbnails[drinkid])

        for i in range(15):
            del(self.ingredients[i][drinkid])
            del(self.measures[i][drinkid])


    def get_drinks_by_name(self, name):
        """
        Gets the ID of the drinks containing the string <name>, or
        returns None if no such drinks exist.

        Args:
            name: The name of the drink whose ID is desired.

        Returns:
            key: The IDs if the drinks containing the string <name>
                 if such drinks exist, otherwise None.
        """
        '''
        Spaces in drink names will be replaced with underscores when
        putting them into the URL, so underscores in drinks passed by
        the server should be replaced by spaces. This means that drink
        names cannot contain underscores, but this is an acceptable
        limitation.
        '''
        name = name.replace("_", " ")

        drinks = []

        for key, value in self.drink_names.items():
            if name.lower() in value.lower():
                drinks.append(key)

        return drinks

    def get_drinks_by_ingredient(self, ingredient):
        """
        Gets all drinks containing the specified ingredient.

        Args:
            ingredient: The ingredient to search for.

        Returns:
            drink_list: List containing the IDs of all drinks containing
                        <ingredient>.
        """
        drinks = []

        # Underscores in ingredient names need to be replaced with
        # spaces for the same reason this was done in get_drink_by_name
        ingredient = ingredient.replace("_", " ")

        # Search through all 15 ingredient dicts.
        for i in range(15):
            for key, value in self.ingredients[i].items():
                # Make sure there is actually a value in this particular
                # ingredient dict (if there isn't it will be None).
                if value:
                    if value.lower() == ingredient.lower():
                        drinks.append(key)

        return drinks

    def get_drinks(self):
        """
        Returns all drink IDs currently in the drinks database.

        Args:
            None

        Returns:
            self.drink_names.keys(): A view object containing the IDs
                                     of all drinks currently in the
                                     drinks database.
        """
        return self.drink_names.keys()

    def reset_drink(self, drinkid, drink_file):
        """
        Loads the data corresponding to ID=drinkid from the specified
        file and replaces the data currently in the database with the
        new data.

        Args:
            drinkid:    The ID of the drink to be reset.
            drink_file: The file from which the data with which to reset
                        the drink data should come.

        Returns:
            None
        """
        f = open(drink_file)
        y = json.load(f)["drinks"]
        drinkid = int(drinkid)
        f.close()

        for drink in y:
            if int(drink["idDrink"]) == drinkid:
                self.drink_names[drinkid] = drink["strDrink"]
                self.drink_alternate_names[drinkid] = drink["strDrinkAlternate"]
                self.drink_tags[drinkid] = drink["strTags"]
                self.drink_videos[drinkid] = drink["strVideo"]
                self.drink_categories[drinkid] = drink["strCategory"]
                self.iba[drinkid] = drink["strIBA"]
                self.alcoholic[drinkid] = drink["strAlcoholic"]
                self.glass[drinkid] = drink["strGlass"]
                self.instructions[drinkid] = drink["strInstructions"]
                self.thumbnails[drinkid] = drink["strDrinkThumb"]

                for i in range(15):
                    ingredient_string = "strIngredient" + str(i+1)
                    measure_string = "strMeasure" + str(i+1)
                    self.ingredients[i][drinkid] = drink[ingredient_string]
                    self.measures[i][drinkid] = drink[measure_string]
