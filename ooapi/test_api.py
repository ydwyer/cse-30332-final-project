from drinks_library import _drinks_database
import unittest
import os

class TestWebServicePrimer(unittest.TestCase):
    DID = 15106
    DNAME = "Apello"
    DINGREDIENT = "Apple juice"
    DATA_FILE = os.path.join(os.path.dirname(__file__), 'data.dat')
    dd = _drinks_database()
    dd.load_drinks(DATA_FILE)

    def reset_drink(self):
        self.dd.reset_drink(self.DID, self.DATA_FILE)

    def test_get_drink(self):
        self.reset_drink()
        drink = self.dd.get_drink(self.DID)
        # Tests whether the drink returned by get_drink() is what it
        # should be by making sure that it has the right name.
        self.assertEqual(drink[0], self.DNAME)

    def test_get_drinks_by_name(self):
        self.reset_drink()
        drinks = self.dd.get_drinks_by_name(self.DNAME)
        # Tests the function by making sure the Apello drink is returned
        # when searching for "Apello" in get_drinks_by_name().
        self.assertTrue(self.DID in drinks)

    def test_get_drinks_by_ingredient(self):
        self.reset_drink()
        drinks = self.dd.get_drinks_by_ingredient(self.DINGREDIENT)
        # Tests the function by making sure the Apello drink is returned
        # when searching for "Apple juice" in get_drinks_by_ingredient().
        self.assertTrue(self.DID in drinks)

    def test_set_drink(self):
        self.reset_drink()
        drink = self.dd.get_drink(self.DID)
        drink[0] = 'Something Else'
        self.dd.set_drink(self.DID, drink)
        drink = self.dd.get_drink(self.DID)
        # Tests the function by making sure the name is changed
        # following the call to set_drink()
        self.assertEqual(drink[0], 'Something Else')

    def test_delete_drink(self):
        self.reset_drink()
        self.dd.delete_drink(self.DID)
        drink = self.dd.get_drink(self.DID)
        # Tests the function by making sure None is returned when
        # searching for a drink's ID following deleting that drink.
        self.assertEqual(drink, None)

    def test_get_drinks(self):
        self.reset_drink()
        drinks = self.dd.get_drinks()
        # Tests the function by making sure that the list of all drink IDs
        # is realistically lengthy and contains the test drink.
        self.assertTrue(self.DID in drinks)
        self.assertTrue(len(drinks) > 100)

if __name__ == "__main__":
    unittest.main()
