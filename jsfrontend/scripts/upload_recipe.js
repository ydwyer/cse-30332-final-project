console.log('Page load happened!')

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('bsr-clear-button');
clearButton.onmouseup = reloadPage;

function getFormInfo() {
    console.log('Entered get Form Info!')

    // Set variables necessary for network calls
    var machine = 'localhost';
    var portNumber = 51070;

    // Get values from webpage
    var drinkName = document.getElementById('drink-name-text').value;
    var glass = document.getElementById('glass-text').value;
    var ingredients = document.getElementById('ingredients-text').value.split(', ');
    var thumbnail = document.getElementById('thumbnail').value;
    var measures = document.getElementById('measures-text').value.split(', ');

    var alcoholicContent = null;
    if (document.getElementById('radio-alcoholic').checked) {
        alcoholicContent = 'Alcoholic';
    }
    else if (document.getElementById('radio-non-alcoholic').checked) {
        alcoholicContent = 'Non-Alcoholic';
    }

    var instructions = document.getElementById('instructions-text').value;

    console.log('drink name: ' + drinkName);
    console.log(ingredients);
    console.log('alcoholic content: ' + alcoholicContent);
    console.log('instructions: ' + instructions);

    // Construct the drink structure that will be sent to the server
    if (ingredients.length <= 15) {
        drink = {};
        drink['name'] = drinkName;
        drink['alternate name'] = null;
        drink['tags'] = null;
        drink['videos'] = null;
        drink['categories'] = null;
        drink['iba'] = null;
        drink['alcoholic'] = alcoholicContent;
        drink['glass'] = glass;
        drink['instructions'] = instructions;
        drink['thumbnails'] = thumbnail;

        if (ingredients.length < measures.length) {
            var result = document.createTextNode('Error: Number of measures is greater than number of ingredients')
            document.getElementById('result').appendChild(result);
        }
        for (var i = 0; i < ingredients.length; i++) {
            var ingredientKey = 'ingredient' + (i + 1).toString();
            var measureKey = 'measure' + (i + 1).toString();
            drink[ingredientKey] = ingredients[i];
            // If the corresponding measure is not given, set it to null.
			if (measures[i] == undefined) {
				drink[measureKey] = null;
			}
			else {
				drink[measureKey] = measures[i];
			}
        }
        for (var i = ingredients.length + 1; i <= 15; i++) {
            var ingredientKey = 'ingredient' + i.toString();
            var measureKey = 'measure' + i.toString();
            drink[ingredientKey] = null;
            drink[measureKey] = null;
        }

        console.log(drink);

        makeNetworkCallToServer(machine, portNumber, drink);
    }
    else {
        var result = document.createTextNode('Error: Too many ingredients. Please enter no more than 15 ingredients, separated by commas.');
        document.getElementById('result').appendChild(result);
    }
}

function makeNetworkCallToServer(machine, portNumber, drink) {
    console.log('entered make nw call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = 'http://' + machine + ':' + portNumber + '/drinks/';

    xhr.open('POST', url, true);

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        updatePageWithResponse(xhr.responseText);
    }

    // set up onerror, which is triggered when error response is received and
    // must be before send
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(JSON.stringify(drink)); // last step - this makes the request
} // end of make nw call

function updatePageWithResponse(responseText) {
    var responseJson = JSON.parse(responseText);
    console.log(responseJson);

    if (responseJson['result'] == 'success') {
        var result = document.createTextNode('Success! Your recipe was added to the database.');
        document.getElementById('result').appendChild(result);
    }
    else {
        var result = document.createTextNode('Sorry! An error occurred. Please try again.');
        document.getElementById('result').appendChild(result);
    }
} // end of updatePageWithResponse

function reloadPage() {
    window.location.reload();
}
