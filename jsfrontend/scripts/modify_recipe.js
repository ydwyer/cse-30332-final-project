console.log('Page load happened!')

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormID;

var clearButton = document.getElementById('bsr-clear-button');
clearButton.onmouseup = reloadPage;

function getFormID() {
    console.log('Entered get Form ID!')

    // Set variables necessary for network calls
    var machine = 'localhost';
    var portNumber = 51070;

    // Get values from webpage
    var id = document.getElementById('drink-id-text').value;
	
	getOriginalValues(machine, portNumber, id);
}

function getOriginalValues(machine, portNumber, id) {
	console.log('entered getOriginalValues call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = 'http://' + machine + ':' + portNumber + '/drinks/' + id;

    xhr.open('GET', url, true);

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        constructDrinkStruct(machine, portNumber, id, xhr.responseText);
    }

    // set up onerror, which is triggered when error response is received and
    // must be called before send()
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(); // last step - this  makes the request
}

function constructDrinkStruct(machine, portNumber, id, responseText) {
	var responseJson = JSON.parse(responseText);
	console.log('Response JSON:');
	console.log(responseJson);
	
	// Get form values
    var drinkName = document.getElementById('drink-name-text').value;
    var glass = document.getElementById('glass-text').value;
    var ingredients = document.getElementById('ingredients-text').value.split(', ');
    var thumbnail = document.getElementById('thumbnail').value;
    var measures = document.getElementById('measures-text').value.split(', ');

    var alcoholicContent = null;
    if (document.getElementById('radio-alcoholic').checked) {
        alcoholicContent = 'Alcoholic';
    }
    else if (document.getElementById('radio-non-alcoholic').checked) {
        alcoholicContent = 'Non-Alcoholic';
    }

    var instructions = document.getElementById('instructions-text').value;

    console.log('drink name: ' + drinkName);
    console.log(ingredients);
    console.log('alcoholic content: ' + alcoholicContent);
    console.log('instructions: ' + instructions);
	
	// Construct the drink structure that will be sent to the server
	// If a value was specified in the form, use it--otherwise, 
	// use the original value.
    if (ingredients.length <= 15) {
        drink = {};
		if (drinkName.length > 1) {
			drink['name'] = drinkName;
		}
		else {
			drink['name'] = responseJson['name'];
		}

        drink['alternate name'] = null;
        drink['tags'] = null;
        drink['videos'] = null;
        drink['categories'] = null;
        drink['iba'] = null;

		drink['alcoholic'] = alcoholicContent;
		
		if (glass.length > 1) {
			drink['glass'] = glass;
		}
		else {
			drink['glass'] = responseJson['glass'];
		}
		
		if (instructions.length > 1) {
			drink['instructions'] = instructions;
		}
		else {
			drink['instructions'] = responseJson['instructions'];
		}
        
		if (thumbnail.length > 1) {
			drink['thumbnails'] = thumbnail;
		}
		else {
			drink['thumbnails'] = responseJson['thumbnails'];
		}

        if (ingredients.length < measures.length) {
            var result = document.createTextNode('Error: Number of measures is greater than number of ingredients')
            document.getElementById('result').appendChild(result);
			return
        }
		else if (isNaN(id)) { // If the ID is invalid, throw an error.
			console.log("type of id:");
			console.log(typeof id);
			var result = document.createTextNode('Error: ID specified is invalid.');
            document.getElementById('result').appendChild(result);
			return
		}
		
		if (ingredients.length > 1) { // If ingredients and measures are
									  // specified in the form
			console.log('Ingredients updated');
			for (var i = 0; i < ingredients.length; i++) {
				var ingredientKey = 'ingredient' + (i + 1).toString();
				var measureKey = 'measure' + (i + 1).toString();
				drink[ingredientKey] = ingredients[i];
				// If the corresponding measure is not given, set it to null.
				if (measures[i] == undefined) {
					drink[measureKey] = null;
				}
				else {
					drink[measureKey] = measures[i];
				}
			}
			for (var i = ingredients.length + 1; i <= 15; i++) {
				var ingredientKey = 'ingredient' + i.toString();
				var measureKey = 'measure' + i.toString();
				drink[ingredientKey] = null;
				drink[measureKey] = null;
			}
		}
		else { // If ingredients and measures are not specified,
			   // use the old values.
			console.log('Ingredients not updated');
			for (var i = 0; i < 15; i++) {
				var ingredientKey = 'ingredient' + (i + 1).toString();
				var measureKey = 'measure' + (i + 1).toString();
				drink[ingredientKey] = responseJson[ingredientKey];
				drink[measureKey] = responseJson[measureKey];
			}
		}

        console.log(drink);

        makeNetworkCallToServer(machine, portNumber, drink, id);
    }
    else {
        var result = document.createTextNode('Error: Too many ingredients. Please enter no more than 15 ingredients, separated by commas.');
        document.getElementById('result').appendChild(result);
    }
}

function makeNetworkCallToServer(machine, portNumber, drink, id) {
    console.log('entered make nw call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = 'http://' + machine + ':' + portNumber + '/drinks/' + id;

    xhr.open('PUT', url, true);

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        updatePageWithResponse(xhr.responseText);
    }

    // set up onerror, which is triggered when error response is received and
    // must be called before send()
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(JSON.stringify(drink)); // last step - this  makes the request
} // end of make nw call

function updatePageWithResponse(responseText) {
    var responseJson = JSON.parse(responseText);
    console.log(responseJson);

    if (responseJson['result'] == 'success') {
        var result = document.createTextNode('Success! The specified recipe was modified.');
        document.getElementById('result').appendChild(result);
    }
    else {
        var result = document.createTextNode('Sorry! An error occurred. Please try again.');
        document.getElementById('result').appendChild(result);
    }
} // end of updateAgeWithResponse

function reloadPage() {
    window.location.reload();
}
