console.log('page load - entered drink_page.js');

var machine = "localhost";
var portNumber = 51070;
var url = window.location.href.split('=');
var drink_id = url[url.length - 1];
console.log('drink_id: ' + drink_id);

makeNetworkCallToServer(machine, portNumber, drink_id);

function makeNetworkCallToServer(machine, portNumber, drink_id){
    console.log('entered make nw call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://" + machine + ":" + portNumber + "/drinks/";

    xhr.open("GET", url + drink_id, true);

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        //console.log(xhr.responseText);
        // do something
        updatePageWithResponse(xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(); // last step - this actually makes the request
} // end of make nw call

function updatePageWithResponse(response_text){
    console.log('entered updatePageWithResponse')
    var response_json = JSON.parse(response_text);

    // update labels
    var label1 = document.getElementById("name-label");
	label1.innerHTML = response_json["name"];

    var label2 = document.getElementById("glass-label");
    label2.innerHTML = "Glass: " + response_json["glass"];

    var id_label = document.getElementById("id-label")
    id_label.innerHTML = "ID: " + response_json["id"];

    var label3 = document.getElementById("instructions-label");
    label3.innerHTML = response_json["instructions"];

    console.log(response_json["thumbnails"])
    if (response_json["thumbnails"] == null || response_json["thumbnails"].length < 2) {
        document.getElementById("img").src="no_image_available.jpg";
    }
    else {
        document.getElementById("img").src=response_json["thumbnails"];
    }

    if (response_json["ingredient1"]) {
        var ingredient_label_1 = document.getElementById("ingredient-label1");
        ingredient_label_1.innerHTML = response_json["ingredient1"];
        var measure_label_1 = document.getElementById("measure-label1");
        measure_label_1.innerHTML = response_json["measure1"];
    }

    if (response_json["ingredient2"]) {
        var ingredient_label_2 = document.getElementById("ingredient-label2");
        ingredient_label_2.innerHTML = response_json["ingredient2"];
        var measure_label_2 = document.getElementById("measure-label2");
        measure_label_2.innerHTML = response_json["measure2"];
    }

    if (response_json["ingredient3"]) {
      var ingredient_label_3 = document.getElementById("ingredient-label3");
      ingredient_label_3.innerHTML = response_json["ingredient3"];
      var measure_label_3 = document.getElementById("measure-label3");
      measure_label_3.innerHTML = response_json["measure3"];
    }

    if (response_json["ingredient4"]) {
        var ingredient_label_4 = document.getElementById("ingredient-label4");
        ingredient_label_4.innerHTML = response_json["ingredient4"];
        var measure_label_4 = document.getElementById("measure-label4");
        measure_label_4.innerHTML = response_json["measure4"];
    }
    if (response_json["ingredient5"]) {
        var ingredient_label_5 = document.getElementById("ingredient-label5");
        ingredient_label_5.innerHTML = response_json["ingredient5"];
        var measure_label_5 = document.getElementById("measure-label5");
        measure_label_5.innerHTML = response_json["measure5"];
    }
    if (response_json["ingredient6"]) {
        var ingredient_label_6 = document.getElementById("ingredient-label6");
        ingredient_label_6.innerHTML = response_json["ingredient6"];
        var measure_label_6 = document.getElementById("measure-label6");
        measure_label_6.innerHTML = response_json["measure6"];
    }
    if (response_json["ingredient7"]) {
        var ingredient_label_7 = document.getElementById("ingredient-label7");
        ingredient_label_8.innerHTML = response_json["ingredient7"];
        var measure_label_7 = document.getElementById("measure-label7");
        measure_label_7.innerHTML = response_json["measure7"];
    }
    if (response_json["ingredient8"]) {
        var ingredient_label_8 = document.getElementById("ingredient-label8");
        ingredient_label_8.innerHTML = response_json["ingredient8"];
        var measure_label_8 = document.getElementById("measure-label8");
        measure_label_8.innerHTML = response_json["measure8"];
    }
    if (response_json["ingredient9"]) {
        var ingredient_label_9 = document.getElementById("ingredient-label9");
        ingredient_label_9.innerHTML = response_json["ingredient9"];
        var measure_label_9 = document.getElementById("measure-label9");
        measure_label_9.innerHTML = response_json["measure9"];
    }
    if (response_json["ingredient10"]) {
        var ingredient_label_10 = document.getElementById("ingredient-label10");
        ingredient_label_10.innerHTML = response_json["ingredient10"];
        var measure_label_10 = document.getElementById("measure-label10");
        measure_label_10.innerHTML = response_json["measure10"];
    }
    if (response_json["ingredient11"]) {
        var ingredient_label_11 = document.getElementById("ingredient-label11");
        ingredient_label_11.innerHTML = response_json["ingredient11"];
        var measure_label_11 = document.getElementById("measure-label11");
        measure_label_11.innerHTML = response_json["measure11"];
    }
    if (response_json["ingredient12"]) {
        var ingredient_label_12 = document.getElementById("ingredient-label12");
        ingredient_label_12.innerHTML = response_json["ingredient12"];
        var measure_label_12 = document.getElementById("measure-label12");
        measure_label_12.innerHTML = response_json["measure12"];
    }
    if (response_json["ingredient13"]) {
        var ingredient_label_13 = document.getElementById("ingredient-label13");
        ingredient_label_13.innerHTML = response_json["ingredient13"];
        var measure_label_13 = document.getElementById("measure-label13");
        measure_label_13.innerHTML = response_json["measure13"];
    }
    if (response_json["ingredient14"]) {
        var ingredient_label_14 = document.getElementById("ingredient-label14");
        ingredient_label_14.innerHTML = response_json["ingredient14"];
        var measure_label_14 = document.getElementById("measure-label14");
        measure_label_14.innerHTML = response_json["measure14"];
    }
    if (response_json["ingredient15"]) {
        var ingredient_label_15 = document.getElementById("ingredient-label15");
        ingredient_label_15.innerHTML = response_json["ingredient15"];
        var measure_label_15 = document.getElementById("measure-label15");
        measure_label_15.innerHTML = response_json["measure15"];
    }
} // end of updatePageWithResponse
