console.log('page load - entered main.js');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('bsr-clear-button');
clearButton.onmouseup = reloadPage;

function getFormInfo() {
    console.log('entered getFormInfo!');

    // Set variables necessary for network call
    var machine = 'localhost';
    var portNumber = 51070;

    // Get values from form
    var drinkName = null;
    if (document.getElementById('radio-drink-name').checked) {
        drinkName = document.getElementById('input-text-4035765').value;
        drinkName = drinkName.replace(' ', '_');
        console.log('drinkName: ' + drinkName);
    }

    var ingredient = null;
    if (document.getElementById('radio-ingredient').checked) {
        ingredient = document.getElementById('input-text-4035765').value;
        ingredient = ingredient.replace(' ', '_');
        console.log('ingredient: ' + ingredient);
    }

    makeNetworkCallToServer(machine, portNumber, drinkName, ingredient);
} // end of get form info

function makeNetworkCallToServer(machine, portNumber, drinkName, ingredient) {
    console.log('entered make nw call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = 'http://' + machine + ':' + portNumber + '/drinks/';

    if (drinkName != null) {
        xhr.open('GET', url + 'name/' + drinkName, true);
    }
    else if (ingredient != null) {
        xhr.open('GET', url + 'ingredient/' + ingredient, true);
    }

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        updatePageWithResponse(xhr.responseText);
    }

    // set up onerror, which is triggered when an error response is received
    // and must be before send()
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(); // last step - this actually makes the request
} // end of make nw call

function updatePageWithResponse(responseText) {
    var responseJson = JSON.parse(responseText);
    console.log(responseJson);

    // Display all the matching drinks under the search bar
    if (responseJson['drinks'].length == 0) {
        var result = document.createTextNode('Sorry! No drinks were found.');
        document.getElementById('result').appendChild(result);
    }
    else {
        for (var i = 0; i < responseJson['drinks'].length; i++) {
            div1 = document.createElement('div');
            div1.setAttribute('class', 'media');

            div2 = document.createElement('div');
            div2.setAttribute('class', 'media-left');
            div1.appendChild(div2);

            a1 = document.createElement('a');
            a1.setAttribute('href', '');
            div2.appendChild(a1);

            img = document.createElement('img');
            img.setAttribute('class', 'media-object');
            if (responseJson['drinks'][i]['thumbnails'] == null) {
                img.setAttribute('src', 'no_image_available.jpg');
            }
            else if (responseJson['drinks'][i]['thumbnails'].length < 2) {
                img.setAttribute('src', 'no_image_available.jpg');
            }
            else {
                img.setAttribute('src', responseJson['drinks'][i]['thumbnails']);
            }

            img.setAttribute('style', 'width:200px;height:200px;');
            img.setAttribute('alt', '');
            a1.appendChild(img);

            div3 = document.createElement('div');
            div3.setAttribute('class', 'media-body');
            div1.appendChild(div3);

            h5 = document.createElement('h5');
            h5.setAttribute('class', 'media-heading');
            div3.appendChild(h5);

            a2 = document.createElement('a');
            var drink_id = responseJson['drinks'][i]['id']
            var drink_page_url = 'index-drink-page.html?drink_id=' + drink_id
            a2.setAttribute('href', drink_page_url);
            h5.appendChild(a2);

            var rJsonName = responseJson['drinks'][i]['name'];
            var drinkName = document.createTextNode(rJsonName);
            a2.appendChild(drinkName);

            document.getElementById('wrapper').appendChild(div1);
        }
    }
} // end of updateAgeWithResponse

function reloadPage() {
    window.location.reload();
}
