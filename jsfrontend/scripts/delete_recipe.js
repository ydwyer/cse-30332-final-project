console.log('Page load happened!')

var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('bsr-clear-button');
clearButton.onmouseup = reloadPage;

function getFormInfo() {
    console.log('Entered get Form Info!')

    // Set values necessary for network calls
    var machine = 'localhost';
    var portNumber = 51070;

    // Get the drink id from the web form
    var id = document.getElementById('drink-id-text').value;

    console.log('drink ID: ' + id);

    makeFirstNetworkCallToServer(machine, portNumber, id);
}

function makeFirstNetworkCallToServer(machine, portNumber, id) {
    console.log('entered make first nw call');

    // set up url
    var xhr = new XMLHttpRequest(); // create request object
    var url = 'http://' + machine + ':' + portNumber + '/drinks/'

    xhr.open('GET', url, true);

    // Run the first network call, which checks whether the specified id exists
    xhr.onload = function(e) {
        processFirstNetworkCall(machine, portNumber, id, xhr.responseText);
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send();
}

function processFirstNetworkCall(machine, portNumber, id, responseText) {
    console.log('entered process first nw call');

    var responseJson = JSON.parse(responseText);

    // Check if the ID we want to delete exists
    var i;
    var valid_id = false;
    console.log('Searching for ID in database');
    for (i = 0; i < responseJson['drinks'].length; i++) {
        if (responseJson['drinks'][i]['id'] == id) {
            console.log('ID found in database');
            valid_id = true;
            break;
        }
    }

    // If said ID does not exist, notify the user and cease execution
    if (!valid_id) {
        console.log('ID not found in database');
        var result = document.createTextNode('The specified ID does not exist in the database.');
        document.getElementById('result').appendChild(result);
        return;
    }

    // If the ID does exist, run the second network call, which will tell the
    // server to delete the drink with said ID from the database.
    makeSecondNetworkCallToServer(machine, portNumber, id);
}

function makeSecondNetworkCallToServer(machine, portNumber, id) {
    console.log('entered make second nw call');

    var url = 'http://' + machine + ':' + portNumber + '/drinks/' + id;

    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', url, true);

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        updatePageWithResponse(xhr.responseText);
    }

    // set up onerror, which triggers the specified function when an error is
    // received and must be before send()
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(); // last step - this actually makes the request
} // end of make nw call

function updatePageWithResponse(responseText) {
    var responseJson = JSON.parse(responseText);
    console.log(responseJson);

    if (responseJson['result'] == 'success') {
        var result = document.createTextNode('Success! The specified recipe has been deleted');
        document.getElementById('result').appendChild(result);
    }
    else {
        var result = document.createTextNode('Sorry! An error occurred. Please try again.');
        document.getElementById('result').appendChild(result);
    }
} // end of updatePageWithResponse

function reloadPage() {
    window.location.reload();
}
